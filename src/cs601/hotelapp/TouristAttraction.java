package cs601.hotelapp;

/**
 * Created by Alec on 10/16/2016.
 */
public class TouristAttraction {
    private String attractionId;
    private String name;
    private String address;
    private double rating;

    public TouristAttraction(String attractionId, String name, String address, double rating){
        this.attractionId = attractionId;
        this.name = name;
        this.address = address;
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public String getAttractionId() {
        return attractionId;
    }

//    public String toString(){
//    }

}
