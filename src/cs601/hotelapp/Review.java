package cs601.hotelapp;

import java.nio.file.Path;
import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Review implements Comparable<Review> {

    private String hotelId;
    private String reviewId;
    private int rating;
    private String reviewTitle;
    private String review;
    private boolean isRecommended;
    private String date;
    private String username;


    public Review(String hotelId, String reviewId, int rating, String reviewTitle, String review, boolean isRecommended, String date, String username){
        this.hotelId = hotelId;
        this.reviewId = reviewId;
        this.rating = rating;
        this.reviewTitle = reviewTitle;
        this.review = review;
        this.isRecommended = isRecommended;
        this.date = date;
        this.username = username;
    }


    @Override
	public int compareTo(Review r) {

        SimpleDateFormat format_type = new SimpleDateFormat("yyyy-MM-dd");

        try {

            Date dateType_date = format_type.parse(date);
            Date dateType_r_date = format_type.parse(r.date);

            int dif = dateType_date.compareTo(dateType_r_date);
            int username_dif;
            int reviewId_dif;

            if (dif == 0){

                username_dif = username.compareTo(r.username);

                if (username_dif == 0){

                    reviewId_dif = reviewId.compareTo(r.reviewId);

                    return reviewId_dif;

                }else {

                    return username_dif;
                }
            }
            else{

                return dif;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }

	}


	//TODO: need getter or setter methods


    public String getHotelId(){
        return hotelId;
    }

    public void setHotelId(String hotelId){
        this.hotelId = hotelId;
    }


    public String getReviewId(){
        return reviewId;
    }

    public void setReviewId(String reviewId){
        this.reviewId = reviewId;
    }


    public int getRating(){
        return rating;
    }

    public void setRating(int rating){
        this.rating = rating;
    }


    public String getReviewTitle(){
        return reviewTitle;
    }

    public void setReviewTitle(String reviewTitle){
        this.reviewTitle = reviewTitle;
    }


    public String getReview(){
        return review;
    }

    public void setReview(String review){
        this.review = review;
    }


    public boolean getIsRecommended(){
        return isRecommended;
    }

    public void setIsRecommended(boolean isRecommended){
        this.isRecommended = isRecommended;
    }


    public String getDate(){
        return date;
    }

    public void setDate(String date){
        this.date = date;
    }


    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

}
