package cs601;

/**
 * Created by hsu022210 on 11/22/16.
 */
public class HtmlTemplate {

    public String header(String title){
        String result = "<!DOCTYPE html>\n" +
                " <html lang=\"en\">\n" +
                "  <head><meta charset=\"utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n " +
                "  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">\n " +
                "  <link rel=\"stylesheet\"\n " +
                "href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" " +
                "integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" " +
                "crossorigin=\"anonymous\">\n" +
                "  <title>%s</title>\n" +
                " </head>\n";
        return String.format(result, title);
    }

    public String footer(){
        String result = "      <!-- jQuery first, then Bootstrap JS. -->\n" +
                "      <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js\">\n" +
                "      </script>\n" +
                "      <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js\" " +
                "integrity=\"sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7\" " +
                "crossorigin=\"anonymous\">" +
                "      </script>\n";
//                "   </body>\n" +
//                "</html>";
        return result;
    }

}
