package cs601;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import cs601.hotelapp.HotelData;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.*;
import java.net.*;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * SimpleServer ("Echo" Server). Opens a welcoming socket, listens for client
 * requests. Once the client asks to connect, creates a new "connection socket"
 * for the client. Reads messages from the client from the input stream. Prints
 * them to the console.
 *
 * Modified from the example of Prof. Engle.
 *
 */
public class RawSocketsHttpServer extends Thread {
    public static final int PORT = 5050;
    public static final String EOT = "EOT";
    public static final String EXIT = "SHUTDOWN";

    private boolean alive; // whether the server is alive or shutdown

    public RawSocketsHttpServer() {
        alive = true;
    }

    public void run() {
        ServerSocket welcomingSocket = null;
        Socket connectionSocket = null;

        Pattern p = Pattern.compile("GET /(.*?)\\?(.*?)=([0-9a-z]+)(&(.*?)=(.*?))* HTTP/1.1");

        HotelData data = new HotelData();
        data.loadHotelInfo("input/hotels200.json");
        data.loadReviews(Paths.get("input/reviews"));

        try {
            // for listening for connection request from clients
            welcomingSocket = new ServerSocket(PORT);
            while (alive) {
                System.out.println("Server: Waiting for connection...");
                // Waits for a client to connect, creates a new connection
                // socket for talking to this client.
                connectionSocket = welcomingSocket.accept();
                System.out.println("Server: Client connected.");
                // Note: welcomingSocket will continue listening for connections
                // from other clients

                BufferedReader reader = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                // The server can now read lines sent by the client using BufferedReader

                // For writing to the socket (so that the server could get client
                // messages)
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(connectionSocket.getOutputStream()));

                String s;
                String hotelId;
                String numString;
                int num;
                String body = "";
                String header = "HTTP/1.1 404 Not Found\r\n";

                while ((s = reader.readLine()) != null) {
                    Matcher m = p.matcher(s);

                    if (s.startsWith("POST")){
                        header = "HTTP/1.1 405 Method Not Allowed\r\n";
                        System.out.println(header);
                    }else{
                        while (m.find()){

                            hotelId = m.group(3);
                            numString = m.group(6);
                            if (hotelId == null){
                                hotelId = "empty";
                            }

                            if (numString == null){
                                numString ="-1";
                            }

                            hotelId = StringEscapeUtils.escapeHtml4(hotelId);

                            if (m.group(1).equals("reviews")){

                                header = "HTTP/1.1 200 OK\r\n";
                                num = Integer.parseInt(numString);
                                body = data.getReviewsJson(hotelId, num);

                            }else if (m.group(1).equals("hotelInfo")){
                                header = "HTTP/1.1 200 OK\r\n";
                                body = data.getHotelInfoJson(hotelId);

                            }else{
                                body = data.getHotelInfoJson(hotelId);
                                System.out.println(header);
                            }


//                        System.out.println("m1" + m.group(1));
//                        System.out.println("m2" + m.group(2));
//                        System.out.println("m3" + m.group(3));
//                        System.out.println("m4" + m.group(4));
//                        System.out.println("m5" + m.group(5));
//                        System.out.println("m6" + m.group(6));
                        }
                        if (s.isEmpty()) {
                            break;
                        }
                    }
                }

                writer.write(header);

//                writer.write("Server: localhost/\r\n");
                writer.write("Content-Type: application/json\r\n");
                writer.write("\r\n");
                writer.write(body);
                writer.flush();

//                System.err.println("Connection socket error");
                writer.close();
                reader.close();
                connectionSocket.close();
//                welcomingSocket.close();
            }

        } catch (Exception ex) {
            System.out.println("Exception occurred while using the socket.");
            ex.printStackTrace();
        } finally {
            try {
                if (welcomingSocket != null && !welcomingSocket.isClosed())
                    welcomingSocket.close();
                if (connectionSocket != null && !connectionSocket.isClosed())
                    connectionSocket.close();
            } catch (IOException e) {
                System.out.println("Could not close the socket");
            }
        }
    }

    public static void main(String[] args) {
        RawSocketsHttpServer server = new RawSocketsHttpServer();
        server.start();
        try {
            server.join();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException occurred " + e);
        }

    }
}