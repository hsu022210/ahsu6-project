package cs601;

/**
 * Created by hsu022210 on 11/12/16.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Paths;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cs601.hotelapp.*;
import org.apache.commons.lang3.StringEscapeUtils;
import java.text.DecimalFormat;
import java.util.concurrent.atomic.DoubleAccumulator;

import cs601.DatabaseHandler;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class AddSavedHotelsServlet extends HttpServlet {

    // DatabaseHandler interacts with the MySQL database
    private static final DatabaseHandler dbhandler = DatabaseHandler.getInstance();

    /**
     * A method that gets executed when the get request is sent to the
     * HelloServlet
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(false);

        if (session != null){
            String hotelId = request.getParameter("hotelId");
            String username = (String) session.getAttribute("username");
            String url = "/hotelDetail?hotelId=" + hotelId;

            Status status = dbhandler.addPersonalPreference(username, hotelId, "savedHotels");

            if (status == Status.OK){
                url = response.encodeRedirectURL(url);
                response.sendRedirect(url);
            }else {
                System.out.println(status.message());
            }

        }else {
            String url = "/login";
            url = response.encodeRedirectURL(url);
            response.sendRedirect(url);
        }
    }
}
