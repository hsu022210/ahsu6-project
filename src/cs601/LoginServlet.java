package cs601;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

/** 
 * A servlet that handles user registration. doGet() method displays an HTML form with a button and
 * two textfields: one for the username, one for the password.
 * doPost() processes the form: if the username is not taken, it adds user info to the database.
 *
 */
@SuppressWarnings("serial")
public class LoginServlet extends BaseServlet {
	
	// DatabaseHandler interacts with the MySQL database
	private static final DatabaseHandler dbhandler = DatabaseHandler.getInstance();


	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		PrintWriter out = response.getWriter();

		HttpSession session=request.getSession(false);

		VelocityEngine ve = (VelocityEngine)request.getServletContext().getAttribute("templateEngine");
		VelocityContext context = new VelocityContext();
		Template template = ve.getTemplate("templates/login.html");
		
		// error will not be null if we were forwarded her from the post method where something went wrong
		String error = request.getParameter("error");

		if(error != null) {
			String errorMessage = getStatusMessage(error);

			context.put("displayMessage", true);
			context.put("message", errorMessage);
		}

		if (session != null){
			String username = (String) session.getAttribute("username");
			String message = "user: " + username + " have already logged in!";

			context.put("displayMessage", true);
			context.put("message", message);
			context.put("homeButton", true);

		}else {
			context.put("loginForm", true);
		}

		StringWriter writer = new StringWriter();
		template.merge(context, writer);

		out.println(writer.toString());
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		// Get data from the textfields of the html form
		String newuser = request.getParameter("user");
		String newpass = request.getParameter("pass");

		// sanitize user input to avoid XSS attacks:
		newuser = StringEscapeUtils.escapeHtml4(newuser);
		newpass = StringEscapeUtils.escapeHtml4(newpass);
		
		// add user's info to the database 
		Status status = dbhandler.loginUser(newuser, newpass);

		if(status == Status.OK) {
			// registration was successful
			HttpSession session=request.getSession();
			session.setAttribute("username", newuser);

			String url = "/hotelList";
			url = response.encodeRedirectURL(url);
			response.sendRedirect(url);
		}
		else {
			// if something went wrong
			String url = "/login?error=" + status.name();
			url = response.encodeRedirectURL(url);
			response.sendRedirect(url);
		}
	}
}