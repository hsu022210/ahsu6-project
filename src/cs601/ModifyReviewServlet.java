package cs601;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.TreeMap;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

/** 
 * A servlet that handles user registration. doGet() method displays an HTML form with a button and
 * two textfields: one for the username, one for the password.
 * doPost() processes the form: if the username is not taken, it adds user info to the database.
 *
 */
@SuppressWarnings("serial")
public class ModifyReviewServlet extends BaseServlet {
	
	// DatabaseHandler interacts with the MySQL database
	private static final DatabaseHandler dbhandler = DatabaseHandler.getInstance();

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		PrintWriter out = response.getWriter();

		HttpSession session=request.getSession(false);

		VelocityEngine ve = (VelocityEngine)request.getServletContext().getAttribute("templateEngine");
		VelocityContext context = new VelocityContext();
		Template template = ve.getTemplate("templates/modifyReview.html");
		
		// error will not be null if we were forwarded her from the post method where something went wrong
		String error = request.getParameter("error");

		if(error != null) {
			String errorMessage = getStatusMessage(error);

			context.put("displayMessage", true);
			context.put("error", true);
			context.put("message", errorMessage);
		}

		if (session != null){
			String reviewId = request.getParameter("reviewId");
			String hotelName = request.getParameter("hotelName");

			TreeMap reviewMap = dbhandler.getSingleReviewMap(reviewId);

			context.put("reviewForm", true);
			context.put("reviewId", reviewId);
			context.put("reviewMap", reviewMap);
			context.put("hotelName", hotelName);
		}else {
			String url = "/login";
			url = response.encodeRedirectURL(url);
			response.sendRedirect(url);
		}

		StringWriter writer = new StringWriter();
		template.merge(context, writer);

		out.println(writer.toString());
	}


	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		PrintWriter out = response.getWriter();

		HttpSession session=request.getSession(false);

		VelocityEngine ve = (VelocityEngine)request.getServletContext().getAttribute("templateEngine");
		VelocityContext context = new VelocityContext();
		Template template = ve.getTemplate("templates/modifyReview.html");

		// Get data from the textfields of the html form
		String reviewId = request.getParameter("reviewId");

		String ratingInString = request.getParameter("rating");
		int rating = Integer.parseInt(ratingInString);

		String reviewTitle = request.getParameter("reviewTitle");
		String review = request.getParameter("review");

		String isRecommendedInString = request.getParameter("isRecommended");
		Boolean isRecommended;
		if (isRecommendedInString.equals("yes")){
			isRecommended = true;
		}else {
			isRecommended = false;
		}

		String date = getDate();
		String username = (String) session.getAttribute("username");


		// sanitize user input to avoid XSS attacks:
		reviewTitle = StringEscapeUtils.escapeHtml4(reviewTitle);
		review = StringEscapeUtils.escapeHtml4(review);

		// add user's info to the database 
		Status status = dbhandler.modifyReview(reviewId, rating, reviewTitle, review, isRecommended, date, username);

		if(status == Status.OK) { // registration was successful
			String message = "Review edited! Database updated.";

			context.put("displayMessage", true);
			context.put("success", true);
			context.put("message", message);
			context.put("homeButton", true);
		}
		else { // if something went wrong
			String url = "/modifyReview?error=" + status.name();
			url = response.encodeRedirectURL(url);
			response.sendRedirect(url); // send a get request  (redirect to the same path)
		}

		StringWriter writer = new StringWriter();
		template.merge(context, writer);

		out.println(writer.toString());
	}
}