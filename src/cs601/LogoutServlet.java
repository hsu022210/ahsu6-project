package cs601;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

/** 
 * A servlet that handles user registration. doGet() method displays an HTML form with a button and
 * two textfields: one for the username, one for the password.
 * doPost() processes the form: if the username is not taken, it adds user info to the database.
 *
 */
@SuppressWarnings("serial")
public class LogoutServlet extends BaseServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		PrintWriter out = response.getWriter();

		HttpSession session=request.getSession(false);

		if (session != null){
			session.invalidate();
			String message = "Successfully logout!";

			VelocityEngine ve = (VelocityEngine)request.getServletContext().getAttribute("templateEngine");
			VelocityContext context = new VelocityContext();
			Template template = ve.getTemplate("templates/logout.html");

			context.put("message", message);

			StringWriter writer = new StringWriter();
			template.merge(context, writer);

			out.println(writer.toString());

		}else {
			String url = "/login";
			url = response.encodeRedirectURL(url);
			response.sendRedirect(url);
		}

	}

}