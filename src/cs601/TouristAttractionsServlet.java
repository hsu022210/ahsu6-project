package cs601;

/**
 * Created by hsu022210 on 11/12/16.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cs601.hotelapp.*;

import cs601.DatabaseHandler;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class TouristAttractionsServlet extends HttpServlet {

    // DatabaseHandler interacts with the MySQL database
    private static final DatabaseHandler dbhandler = DatabaseHandler.getInstance();

    /**
     * A method that gets executed when the get request is sent to the
     * HelloServlet
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession(false);

        VelocityEngine ve = (VelocityEngine)request.getServletContext().getAttribute("templateEngine");
        VelocityContext context = new VelocityContext();
        Template template = ve.getTemplate("templates/attractions.html");

        if (session != null){
            ThreadSafeHotelData data = (ThreadSafeHotelData) getServletContext().getAttribute("hotelData");
            HotelDataBuilder hotelDataBuilder = (HotelDataBuilder) getServletContext().getAttribute("hotelDataBuilder");

            String username = (String) session.getAttribute("username");
            String hotelId = request.getParameter("hotelId");
            String radiusInMilesString = request.getParameter("radiusInMiles");

            if (radiusInMilesString == null){
                radiusInMilesString = "100";
                System.out.println("null null null " + radiusInMilesString);
            }else {
                System.out.println("have rad " + radiusInMilesString);
            }

            int radiusInMiles = Integer.parseInt(radiusInMilesString);

            hotelDataBuilder.fetchAttractionsForSingleHotel(radiusInMiles, hotelId);

            TreeSet<String> nearHotelAttractionIds = data.getNearHotelAttractionIds(hotelId);

            if (nearHotelAttractionIds != null){
                List attractionsArr = data.getTouristAttractionsArr(nearHotelAttractionIds);
                context.put("attractionsArr", attractionsArr);
            }

            TreeMap hotelDetailMap = dbhandler.getHotelDetail(hotelId);

            context.put("username", username);
            context.put("hotelDetailMap", hotelDetailMap);
            context.put("radiusInMilesString", radiusInMilesString);

        }else {
            String url = "/login";
            url = response.encodeRedirectURL(url);
            response.sendRedirect(url);
        }

        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        out.println(writer.toString());

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String hotelId = request.getParameter("hotelId");
        String radiusInMilesString = request.getParameter("radiusInMiles");
        String url = "/touristAttractions?hotelId=" + hotelId + "&radiusInMiles=" + radiusInMilesString;

        url = response.encodeRedirectURL(url);
        response.sendRedirect(url);
    }
}
