package cs601;

import cs601.hotelapp.Hotel;
import cs601.hotelapp.HotelData;
import cs601.hotelapp.Review;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.text.DecimalFormat;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

/**
 * Handles all database-related actions. Uses singleton design pattern. Modified
 * by Prof. Karpenko from the original example of Prof. Engle.
 * 
 * @see RegisterServer
 */
public class DatabaseHandler {

	/** Makes sure only one database handler is instantiated. */
	private static DatabaseHandler singleton = new DatabaseHandler();

	/** Used to determine if login_users table exists. */
	private static final String TABLES_SQL = "SHOW TABLES LIKE 'login_users';";

	/** Used to create login_users table for this example. */
	private static final String CREATE_SQL = "CREATE TABLE login_users ("
			+ "userid INTEGER AUTO_INCREMENT PRIMARY KEY, " + "username VARCHAR(32) NOT NULL UNIQUE, "
			+ "password CHAR(64) NOT NULL, " + "usersalt CHAR(32) NOT NULL);";

	/** Used to insert a new user's info into the login_users table */
	private static final String REGISTER_SQL = "INSERT INTO login_users (username, password, usersalt) "
			+ "VALUES (?, ?, ?);";

	/** Used to determine if a username already exists. */
	private static final String USER_SQL = "SELECT username FROM login_users WHERE username = ?";

	// ------------------ constants below will be useful for the login operation
	// once you implement it
	/** Used to retrieve the salt associated with a specific user. */
	private static final String SALT_SQL = "SELECT usersalt FROM login_users WHERE username = ?";

	/** Used to authenticate a user. */
	private static final String AUTH_SQL = "SELECT username FROM login_users " + "WHERE username = ? AND password = ?";

	/** Used to remove a user from the database. */
	private static final String DELETE_SQL = "DELETE FROM login_users WHERE username = ?";

	/** Used to configure connection to database. */
	private DatabaseConnector db;

	/** Used to generate password hash salt for user. */
	private Random random;

	/** Used to insert a new hotel's info into the hotels table */
	private static final String HOTEL_SQL = "INSERT INTO hotels (hotelId, hotelName, streetAddress, city, state, country, latitude, longitude) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

	/** Used to insert a new review into the reviews table */
	private static final String REVIEW_SQL = "INSERT INTO reviews (hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

	/** Used to retrieve hotels. */
	private static final String ALL_HOTELS_SQL = "SELECT hotelId, hotelName, streetAddress, city, state, country, latitude, longitude FROM hotels ";

	/** Used to retrieve review for each hotel. */
	private static final String REVIEWS_FOR_HOTEL_SQL = "SELECT hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username FROM reviews " + "WHERE hotelId = ?";

	/** Used to retrieve review for each hotel. */
	private static final String REVIEWS_FOR_HOTEL_SQL_ORDER_DATE = "SELECT hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username FROM reviews " + "WHERE hotelId = ? " + "ORDER BY date DESC";

	/** Used to retrieve review for each hotel. */
	private static final String REVIEWS_FOR_HOTEL_SQL_ORDER_RATING = "SELECT hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username FROM reviews " + "WHERE hotelId = ? " + "ORDER BY rating DESC";

	/** Used to retrieve review for all hotels. */
	private static final String REVIEWS_FOR_ALL_HOTELS_SQL = "SELECT hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username FROM reviews ";

	/** Used to insert a new review into the reviews table */
	private static final String ADD_REVIEW_SQL = "INSERT INTO reviews (hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

	/** Used to retrieve hotels. */
	private static final String SINGLE_HOTEL_SQL = "SELECT hotelId, hotelName, streetAddress, city, state, country, latitude, longitude FROM hotels " + " WHERE hotelId = ?";

	/** Used to update review */
	private static final String MODIFY_REVIEW_SQL = "UPDATE reviews SET rating = ?, reviewTitle = ?, review = ?, isRecommended = ?, date = ? WHERE reviewId = ?";

	/** Used to retrieve specific review. */
	private static final String GET_A_REVIEW_SQL = "SELECT hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username FROM reviews " + "WHERE reviewId = ?";

	/** Used to retrieve hotels. */
	private static final String HOTELS_SEARCH_NAME_SQL = "SELECT hotelId, hotelName, streetAddress, city, state, country, latitude, longitude FROM hotels WHERE hotelName LIKE ?";

	/** Used to insert a new review into the reviews table */
	private static final String ADD_VISITED_EXPEDIA_LINK_SQL = "INSERT INTO visitedExpedia (username, hotelId) " + "VALUES (?, ?);";

	/** Used to insert a new review into the reviews table */
	private static final String GET_VISITED_EXPEDIA_LINKS_SQL = "SELECT username, hotelId FROM visitedExpedia WHERE username = ?";

	/** Used to insert a new review into the reviews table */
	private static final String ADD_SAVED_HOTEL_SQL = "INSERT INTO savedHotels (username, hotelId) " + "VALUES (?, ?);";

	/** Used to insert a new review into the reviews table */
	private static final String GET_SAVED_HOTELS_SQL = "SELECT username, hotelId FROM savedHotels WHERE username = ?";

	/** Used to remove a user from the database. */
	private static final String CLEAR_VISITED_EXPEDIA_LINKS_SQL = "DELETE FROM visitedExpedia WHERE username = ?";

	/** Used to remove a user from the database. */
	private static final String CLEAR_SAVED_HOTELS_SQL = "DELETE FROM savedHotels WHERE username = ?";

	/** Used to remove a user from the database. */
	private static final String GET_AVG_RATING_HOTELS_SQL = "SELECT hotels.hotelId, hotels.hotelName, AVG(reviews.rating) AS average_rating FROM hotels LEFT JOIN reviews on hotels.hotelId=reviews.hotelId GROUP BY hotels.hotelId;";

	/** Used to remove a user from the database. */
	private static final String GET_AVG_RATING_HOTEL_SQL = "SELECT hotels.hotelId, hotels.hotelName, AVG(reviews.rating) AS average_rating FROM hotels LEFT JOIN reviews on hotels.hotelId=reviews.hotelId WHERE hotels.hotelId = ?;";

	/**
	 * This class is a singleton, so the constructor is private. Other classes
	 * need to call getInstance()
	 */
	private DatabaseHandler() {
		Status status = Status.OK;
		random = new Random(System.currentTimeMillis());

		try {
			db = new DatabaseConnector("database.properties");
			status = db.testConnection() ? setupTables() : Status.CONNECTION_FAILED;
		} catch (FileNotFoundException e) {
			status = Status.MISSING_CONFIG;
		} catch (IOException e) {
			status = Status.MISSING_VALUES;
		}

		if (status != Status.OK) {
			System.out.println("Error while obtaining a connection to the database: " + status);
		}
	}

	/**
	 * Gets the single instance of the database handler.
	 *
	 * @return instance of the database handler
	 */
	public static DatabaseHandler getInstance() {
		return singleton;
	}

	/**
	 * Checks to see if a String is null or empty.
	 * 
	 * @param text
	 *            - String to check
	 * @return true if non-null and non-empty
	 */
	public static boolean isBlank(String text) {
		return (text == null) || text.trim().isEmpty();
	}

	/**
	 * Checks if necessary table exists in database, and if not tries to create
	 * it.
	 *
	 * @return {@link Status.OK} if table exists or create is successful
	 */
	private Status setupTables() {
		Status status = Status.ERROR;

		try (Connection connection = db.getConnection(); Statement statement = connection.createStatement();) {
			if (!statement.executeQuery(TABLES_SQL).next()) {
				// Table missing, must create
				statement.executeUpdate(CREATE_SQL);

				// Check if create was successful
				if (!statement.executeQuery(TABLES_SQL).next()) {
					status = Status.CREATE_FAILED;
				} else {
					status = Status.OK;
				}
			} else {
				status = Status.OK;
			}
		} catch (Exception ex) {
			status = Status.CREATE_FAILED;
		}

		return status;
	}

	/**
	 * Tests if a user already exists in the database. Requires an active
	 * database connection.
	 *
	 * @param connection
	 *            - active database connection
	 * @param user
	 *            - username to check
	 * @return Status.OK if user does not exist in database
	 * @throws SQLException
	 */
	private Status duplicateUser(Connection connection, String user) {

		assert connection != null;
		assert user != null;

		Status status = Status.ERROR;

		try (PreparedStatement statement = connection.prepareStatement(USER_SQL);) {
			statement.setString(1, user);

			ResultSet results = statement.executeQuery();
			status = results.next() ? Status.DUPLICATE_USER : Status.OK;
		} catch (SQLException e) {
			status = Status.SQL_EXCEPTION;
			System.out.println("Exception occured while processing SQL statement:" + e);
		}

		return status;
	}

	/**
	 * Returns the hex encoding of a byte array.
	 *
	 * @param bytes
	 *            - byte array to encode
	 * @param length
	 *            - desired length of encoding
	 * @return hex encoded byte array
	 */
	public static String encodeHex(byte[] bytes, int length) {
		BigInteger bigint = new BigInteger(1, bytes);
		String hex = String.format("%0" + length + "X", bigint);

		assert hex.length() == length;
		return hex;
	}

	/**
	 * Calculates the hash of a password and salt using SHA-256.
	 *
	 * @param password
	 *            - password to hash
	 * @param salt
	 *            - salt associated with user
	 * @return hashed password
	 */
	public static String getHash(String password, String salt) {
		String salted = salt + password;
		String hashed = salted;

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(salted.getBytes());
			hashed = encodeHex(md.digest(), 64);
		} catch (Exception ex) {
			System.out.println("Unable to properly hash password." + ex);
		}

		return hashed;
	}

	/**
	 * Registers a new user, placing the username, password hash, and salt into
	 * the database if the username does not already exist.
	 *
	 * @param newuser
	 *            - username of new user
	 * @param newpass
	 *            - password of new user
	 * @return {@link Status.OK} if registration successful
	 */
	public Status registerUser(String newuser, String newpass) {
		Status status = Status.ERROR;
		System.out.println("Registering " + newuser + ".");

		// make sure we have non-null and non-emtpy values for login
		if (isBlank(newuser) || isBlank(newpass)) {
			status = Status.INVALID_LOGIN;
			System.out.println("Invalid regiser info");
			return status;
		}

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {
			status = duplicateUser(connection, newuser);

			// if okay so far, try to insert new user
			if (status == Status.OK) {
				// generate salt
				byte[] saltBytes = new byte[16];
				random.nextBytes(saltBytes);

				String usersalt = encodeHex(saltBytes, 32); // hash salt
				String passhash = getHash(newpass, usersalt); // combine
																// password and
																// salt and hash
																// again

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(REGISTER_SQL);) {
					statement.setString(1, newuser);
					statement.setString(2, passhash);
					statement.setString(3, usersalt);
					statement.executeUpdate();

					status = Status.OK;
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return status;
	}


	/**
	 * Gets the salt for a specific user.
	 *
	 * @param connection
	 *            - active database connection
	 * @param user
	 *            - which user to retrieve salt for
	 * @return salt for the specified user or null if user does not exist
	 * @throws SQLException
	 *             if any issues with database connection
	 */
	private String getSalt(Connection connection, String user) throws SQLException {
		assert connection != null;
		assert user != null;

		String salt = null;

		try (PreparedStatement statement = connection.prepareStatement(SALT_SQL);) {
			statement.setString(1, user);

			ResultSet results = statement.executeQuery();

			if (results.next()) {
				salt = results.getString("usersalt");
			}
		}

		return salt;
	}


	public Status loginUser(String newuser, String newpass) {
		Status status = Status.ERROR;
		System.out.println("Trying login " + newuser + ".");

		// make sure we have non-null and non-emtpy values for login
		if (isBlank(newuser) || isBlank(newpass)) {
			status = Status.INVALID_LOGIN;
			System.out.println("Invalid login info");
			return status;
		}

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {
			status = duplicateUser(connection, newuser);

			// username not exist
			if (status == Status.OK) {
				status = Status.INVALID_USER;
			}
			// user exists
			else {

				String usersalt = getSalt(connection, newuser);
				String passhash = getHash(newpass, usersalt);

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(AUTH_SQL);) {
					statement.setString(1, newuser);
					statement.setString(2, passhash);
					ResultSet result = statement.executeQuery();

					if (result.next()){
						status = Status.OK;

					}else {
						status = Status.INVALID_LOGIN;

					}
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return status;
	}


	public Status dumpHotelAndReview(HotelData data) {
		Status status = Status.OK;
		System.out.println("Storing hotelData");

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {

			// if okay so far, try to insert new user
			if (status == Status.OK) {


				List<String> hotelIdList = data.getHotels();

				for (String eachHotelId:hotelIdList){
					Hotel eachHotelInfo = data.getHotelInfo(eachHotelId);
					TreeSet<Review> reviews = data.getReviews(eachHotelId);

					// add user info to the database table
					try (PreparedStatement statement = connection.prepareStatement(HOTEL_SQL);) {
						statement.setString(1, eachHotelInfo.getHotelId());
						statement.setString(2, eachHotelInfo.getHotelName());
						statement.setString(3, eachHotelInfo.getAddress().getStreetAddress());
						statement.setString(4, eachHotelInfo.getAddress().getCity());
						statement.setString(5, eachHotelInfo.getAddress().getState());
						statement.setString(6, eachHotelInfo.getAddress().getCountry());
						statement.setDouble(7, eachHotelInfo.getAddress().getLatitude());
						statement.setDouble(8, eachHotelInfo.getAddress().getLongitude());
						statement.executeUpdate();
						System.out.println("store data for "+ eachHotelInfo.getHotelId() + " " + eachHotelInfo.getHotelName());

						status = Status.OK;


					if (reviews != null){
						for (Review r : reviews){
							try (PreparedStatement statementReview = connection.prepareStatement(REVIEW_SQL);) {
								statementReview.setString(1, r.getHotelId());
								statementReview.setString(2, r.getReviewId());
								statementReview.setInt(3, r.getRating());
								statementReview.setString(4, r.getReviewTitle());
								statementReview.setString(5, r.getReview());
								statementReview.setBoolean(6, r.getIsRecommended());
								statementReview.setString(7, r.getDate());
								statementReview.setString(8, r.getUsername());
								statementReview.executeUpdate();
								System.out.println("store review " + r.getReviewId() + " for " + r.getHotelId());

								status = Status.OK;
							}
						}
					}else {
					}
				}


				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return status;
	}


	public ArrayList getHotelsList() {
		ArrayList hotelslist = new ArrayList<TreeMap>();

		ResultSet result = null;

		Status status = Status.OK;
		System.out.println("retrieving hotel list ");

		try (Connection connection = db.getConnection();) {

			try (PreparedStatement statement = connection.prepareStatement(ALL_HOTELS_SQL);) {
				result = statement.executeQuery();
				while (result.next()){
					TreeMap singleHotelMap = new TreeMap();
					singleHotelMap.put("hotelId", result.getString(1));
					singleHotelMap.put("hotelName", result.getString(2));
					singleHotelMap.put("streetAddress", result.getString(3));
					singleHotelMap.put("city", result.getString(4));
					singleHotelMap.put("state", result.getString(5));
					singleHotelMap.put("country", result.getString(6));
					singleHotelMap.put("latitude", result.getString(7));
					singleHotelMap.put("longitude", result.getString(8));

					hotelslist.add(singleHotelMap);
				}
			}

		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return hotelslist;
	}


	public ArrayList getHotelsListSearchName(String hotelName) {
		ArrayList hotelslist = new ArrayList<TreeMap>();

		ResultSet result = null;

		Status status = Status.OK;
		System.out.println("retrieving hotels list for " + hotelName);

		try (Connection connection = db.getConnection();) {

			try (PreparedStatement statement = connection.prepareStatement(HOTELS_SEARCH_NAME_SQL);) {
				String hotelNameLike = "%" + hotelName + "%";
				statement.setString(1, hotelNameLike);
				result = statement.executeQuery();
				while (result.next()){
					TreeMap singleHotelMap = new TreeMap();
					singleHotelMap.put("hotelId", result.getString(1));
					singleHotelMap.put("hotelName", result.getString(2));
					singleHotelMap.put("streetAddress", result.getString(3));
					singleHotelMap.put("city", result.getString(4));
					singleHotelMap.put("state", result.getString(5));
					singleHotelMap.put("country", result.getString(6));
					singleHotelMap.put("latitude", result.getString(7));
					singleHotelMap.put("longitude", result.getString(8));

					hotelslist.add(singleHotelMap);
				}
			}

		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return hotelslist;
	}


	public TreeMap getReviewsMapForHotels() {

		TreeMap hotelReviewMap = new TreeMap<String, ArrayList>();


		ResultSet result;
		Status status = Status.OK;
		System.out.println("retrieving reviews for all hotels");

		try (Connection connection = db.getConnection();) {

			try (PreparedStatement statement = connection.prepareStatement(REVIEWS_FOR_ALL_HOTELS_SQL);) {
				result = statement.executeQuery();
				while (result.next()){

					ArrayList reviewArray;

					if (hotelReviewMap.get(result.getString(1)) == null){

						reviewArray = new ArrayList<TreeMap>();
					}else{

						reviewArray = (ArrayList) hotelReviewMap.get(result.getString(1));
					}

					TreeMap singleReviewMap = new TreeMap();

					singleReviewMap.put("hotelId", result.getString(1));
					singleReviewMap.put("reviewId", result.getString(2));
					singleReviewMap.put("rating", result.getString(3));
					singleReviewMap.put("reviewTitle", result.getString(4));
					singleReviewMap.put("review", result.getString(5));
					singleReviewMap.put("isRecommended", result.getString(6));
					singleReviewMap.put("date", result.getString(7));
					singleReviewMap.put("username", result.getString(8));

					reviewArray.add(singleReviewMap);
					hotelReviewMap.put(result.getString(1), reviewArray);
				}
				System.out.println(status);
			}

		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println(status);
			System.out.println("Error while connecting to the database: " + ex);
		}

		return hotelReviewMap;
	}


	/**
	 * Add a new review for a hotel.
	 */
	public Status addReview(String hotelId, String reviewId, int rating, String reviewTitle, String review,
							 Boolean isRecommended, String date, String username) {
		Status status = Status.OK;
		System.out.println("Adding review for " + hotelId + ".");

		// make sure we have non-null and non-emtpy values for login
		if (isBlank(reviewTitle) || isBlank(review)) {
			status = Status.MISSING_VALUES;
			System.out.println("Missing review or review title");
			return status;
		}

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {

			// if okay so far, try to insert new user
			if (status == Status.OK) {

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(ADD_REVIEW_SQL);) {
//					(hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username)
					statement.setString(1, hotelId);
					statement.setString(2, reviewId);
					statement.setInt(3, rating);
					statement.setString(4, reviewTitle);
					statement.setString(5, review);
					statement.setBoolean(6, isRecommended);
					statement.setString(7, date);
					statement.setString(8, username);
					statement.executeUpdate();

					status = Status.OK;
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return status;
	}


	public Status registerUserForReviewJsons() {
		TreeMap<String, ArrayList> reviewsMapForHotels = getReviewsMapForHotels();
		Status status = Status.OK;
		System.out.println("Adding users for json files");

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {
			String newuser;
			String newpass;

			for (ArrayList<TreeMap> reviewsForSingleHotel: reviewsMapForHotels.values()){
				for (TreeMap reviewMap: reviewsForSingleHotel){

					newuser = (String) reviewMap.get("username");
					status = duplicateUser(connection, newuser);
					newpass = newuser + UUID.randomUUID().toString().substring(1,6);

					// if okay so far, try to insert new user
					if (status == Status.OK) {
						// generate salt
						byte[] saltBytes = new byte[16];
						random.nextBytes(saltBytes);

						String usersalt = encodeHex(saltBytes, 32); // hash salt
						String passhash = getHash(newpass, usersalt); // combine
						// password and
						// salt and hash
						// again

						// add user info to the database table
						try (PreparedStatement statement = connection.prepareStatement(REGISTER_SQL);) {
							statement.setString(1, newuser);
							statement.setString(2, passhash);
							statement.setString(3, usersalt);
							statement.executeUpdate();
							System.out.println("user " + newuser + " added");

							status = Status.OK;
						}
					}

				}
			}

		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return status;
	}

	public TreeMap getHotelDetail(String hotelId) {
		TreeMap hotelDetail = new TreeMap();

		ResultSet result = null;

		Status status = Status.OK;
		System.out.println("retrieving hotel detail ");

		try (Connection connection = db.getConnection();) {

			try (PreparedStatement statement = connection.prepareStatement(SINGLE_HOTEL_SQL);) {
				statement.setString(1, hotelId);
				result = statement.executeQuery();

				while (result.next()){
					hotelDetail.put("hotelId", result.getString(1));
					hotelDetail.put("hotelName", result.getString(2));
					hotelDetail.put("streetAddress", result.getString(3));
					hotelDetail.put("city", result.getString(4));
					hotelDetail.put("state", result.getString(5));
					hotelDetail.put("country", result.getString(6));
					hotelDetail.put("latitude", result.getString(7));
					hotelDetail.put("longitude", result.getString(8));
				}
			}

		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return hotelDetail;
	}

	public ArrayList getHotelReviewsArr(String hotelId, String order) {
		ArrayList hotelReviewsArr = new ArrayList();
		String REVIEWS_SQL;

		if (order.equals("date")){
			REVIEWS_SQL = REVIEWS_FOR_HOTEL_SQL_ORDER_DATE;
		}else {
			REVIEWS_SQL = REVIEWS_FOR_HOTEL_SQL_ORDER_RATING;
		}

		ResultSet result = null;

		Status status = Status.OK;
		System.out.println("retrieving hotel detail ");

		try (Connection connection = db.getConnection();) {

			try (PreparedStatement statement = connection.prepareStatement(REVIEWS_SQL);) {
				statement.setString(1, hotelId);
				result = statement.executeQuery();

				while (result.next()){
					TreeMap hotelReview = new TreeMap();
					hotelReview.put("hotelId", result.getString(1));
					hotelReview.put("reviewId", result.getString(2));
					hotelReview.put("rating", result.getString(3));
					hotelReview.put("reviewTitle", result.getString(4));
					hotelReview.put("review", result.getString(5));
					hotelReview.put("isRecommended", result.getString(6));
					hotelReview.put("date", result.getString(7));
					hotelReview.put("username", result.getString(8));
					hotelReviewsArr.add(hotelReview);

				}
			}

		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return hotelReviewsArr;
	}

	/**
	 * modify a review for a hotel.
	 */
	public Status modifyReview(String reviewId, int rating, String reviewTitle, String review,
							Boolean isRecommended, String date, String username) {
		Status status = Status.OK;
		System.out.println("Modifying review for " + reviewId + ".");

		// make sure we have non-null and non-emtpy values for login
		if (isBlank(reviewTitle) || isBlank(review)) {
			status = Status.MISSING_VALUES;
			System.out.println("Missing review or review title");
			return status;
		}

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {

			// if okay so far, try to insert new user
			if (status == Status.OK) {

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(MODIFY_REVIEW_SQL);) {
//					MODIFY_REVIEW_SQL = "UPDATE reviews SET rating = ?, reviewTitle = ?, review = ?, isRecommended = ?, date = ? WHERE reviewId = ?";
					statement.setInt(1, rating);
					statement.setString(2, reviewTitle);
					statement.setString(3, review);
					statement.setBoolean(4, isRecommended);
					statement.setString(5, date);
					statement.setString(6, reviewId);
					statement.executeUpdate();

					status = Status.OK;
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return status;
	}

	public TreeMap getSingleReviewMap(String reviewId) {

		TreeMap reviewMap = new TreeMap();


		ResultSet result;
		Status status = Status.OK;
		System.out.println("retrieving a review");

		try (Connection connection = db.getConnection();) {

			try (PreparedStatement statement = connection.prepareStatement(GET_A_REVIEW_SQL);) {
				statement.setString(1, reviewId);
				result = statement.executeQuery();
				while (result.next()){
					reviewMap.put("hotelId", result.getString(1));
					reviewMap.put("reviewId", result.getString(2));
					reviewMap.put("rating", result.getString(3));
					reviewMap.put("reviewTitle", result.getString(4));
					reviewMap.put("review", result.getString(5));
					reviewMap.put("isRecommended", result.getString(6));
					reviewMap.put("date", result.getString(7));
					reviewMap.put("username", result.getString(8));

					System.out.println(status);
				}

			}

		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println(status);
			System.out.println("Error while connecting to the database: " + ex);
		}

		return reviewMap;
	}

	/**
	 * Add a new Expedia link for a user.
	 */
	public Status addPersonalPreference(String username, String hotelId, String type) {
		Status status = Status.OK;
		System.out.println("Adding hotel Link for " + username + ".");

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {

			// if okay so far, try to insert new user
			if (status == Status.OK) {

				String QUERY_SQL = "";

				if (type.equals("visitedExpedia")){
					QUERY_SQL = ADD_VISITED_EXPEDIA_LINK_SQL;
				}else if (type.equals("savedHotels")){
					QUERY_SQL = ADD_SAVED_HOTEL_SQL;
				}else{}

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(QUERY_SQL);) {
//					(hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username)
					statement.setString(1, username);
					statement.setString(2, hotelId);
					statement.executeUpdate();

					status = Status.OK;
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return status;
	}


	/**
	 * Retrieve Expedia links for a user.
	 */
	public ArrayList getPersonalPreference(String username, String type) {

		ArrayList visitedExpediaLinksArr = new ArrayList();

		Status status = Status.OK;
		System.out.println("get hotel link for  " + username + ".");

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {

			// if okay so far, try to insert new user
			if (status == Status.OK) {

				String QUERY_SQL = "";

				if (type.equals("visitedExpedia")){
					QUERY_SQL = GET_VISITED_EXPEDIA_LINKS_SQL;
				}else if(type.equals("savedHotels")){
					QUERY_SQL = GET_SAVED_HOTELS_SQL;
				}else {}

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(QUERY_SQL);) {
//					(hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username)
					statement.setString(1, username);
					ResultSet result = statement.executeQuery();

					while (result.next()){
						TreeMap usernameAndHotelId = new TreeMap();
						usernameAndHotelId.put("username", result.getString(1));
						usernameAndHotelId.put("hotelId", result.getString(2));
						TreeMap hotelDetailMap = getHotelDetail(result.getString(2));
						usernameAndHotelId.put("hotelDetailMap", hotelDetailMap);
						visitedExpediaLinksArr.add(usernameAndHotelId);
					}

					status = Status.OK;
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return visitedExpediaLinksArr;
	}

	/**
	 * Add a new Expedia link for a user.
	 */
	public Status clearPersonalPreference(String username, String type) {
		Status status = Status.OK;
		System.out.println("clearing hotel for " + username + "." + type);

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {

			// if okay so far, try to insert new user
			if (status == Status.OK) {

				String QUERY_SQL = "";

				if (type.equals("visitedExpedia")){
					QUERY_SQL = CLEAR_VISITED_EXPEDIA_LINKS_SQL;
				}else if (type.equals("savedHotels")){
					QUERY_SQL = CLEAR_SAVED_HOTELS_SQL;
				}else{}

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(QUERY_SQL);) {
//					(hotelId, reviewId, rating, reviewTitle, review, isRecommended, date, username)
					statement.setString(1, username);
					statement.executeUpdate();

					status = Status.OK;
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return status;
	}


	/**
	 * Retrieve avg rating for a hotel.
	 */
	public TreeMap getAvgRatingForHotelsMap() {

		TreeMap avgRatingForHotelsMap = new TreeMap();

		Status status = Status.OK;
		System.out.println("get avg rating for hotels");

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {

			// if okay so far, try to insert new user
			if (status == Status.OK) {

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(GET_AVG_RATING_HOTELS_SQL);) {
					ResultSet result = statement.executeQuery();
					DecimalFormat df = new DecimalFormat("#.0");

					while (result.next()){
						String avgRatingString;
						if (result.getString(3) == null){
							avgRatingString = "No Rating";
						}else {
							double avgRating = Double.parseDouble(result.getString(3));
							avgRatingString = df.format(avgRating);
						}
						avgRatingForHotelsMap.put(result.getString(1), avgRatingString);
					}

					status = Status.OK;
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return avgRatingForHotelsMap;
	}

	/**
	 * Retrieve avg rating for a hotel.
	 */
	public String getAvgRatingForHotel(String hotelId) {
		String avgRatingString = "";

		Status status = Status.OK;
		System.out.println("get avg rating for " + hotelId);

		// try to connect to database and test for duplicate user
		try (Connection connection = db.getConnection();) {

			// if okay so far, try to insert new user
			if (status == Status.OK) {

				// add user info to the database table
				try (PreparedStatement statement = connection.prepareStatement(GET_AVG_RATING_HOTEL_SQL);) {
					statement.setString(1, hotelId);
					ResultSet result = statement.executeQuery();
					DecimalFormat df = new DecimalFormat("#.0");

					while (result.next()){

						if (result.getString(3) == null){
							avgRatingString = "No Rating";
						}else {
							double avgRating = Double.parseDouble(result.getString(3));
							avgRatingString = df.format(avgRating);
						}
					}


					status = Status.OK;
				}
			}
		} catch (SQLException ex) {
			status = Status.CONNECTION_FAILED;
			System.out.println("Error while connecting to the database: " + ex);
		}

		return avgRatingString;
	}

}
