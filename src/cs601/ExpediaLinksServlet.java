package cs601;

/**
 * Created by hsu022210 on 11/12/16.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Paths;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cs601.hotelapp.Hotel;
import cs601.hotelapp.HotelData;
import cs601.hotelapp.Review;
import org.apache.commons.lang3.StringEscapeUtils;
import java.text.DecimalFormat;
import java.util.concurrent.atomic.DoubleAccumulator;

import cs601.DatabaseHandler;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class ExpediaLinksServlet extends HttpServlet {

    // DatabaseHandler interacts with the MySQL database
    private static final DatabaseHandler dbhandler = DatabaseHandler.getInstance();

    /**
     * A method that gets executed when the get request is sent to the
     * HelloServlet
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession(false);

        VelocityEngine ve = (VelocityEngine)request.getServletContext().getAttribute("templateEngine");
        VelocityContext context = new VelocityContext();
        Template template = ve.getTemplate("templates/expediaLinks.html");

        if (session != null){
            String username = (String) session.getAttribute("username");

            ArrayList visitedExpediaLinksArr = dbhandler.getPersonalPreference(username, "visitedExpedia");

            context.put("username", username);
            context.put("visitedExpediaLinksArr", visitedExpediaLinksArr);

        }else {
            String url = "/login";
            url = response.encodeRedirectURL(url);
            response.sendRedirect(url);
        }

        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        out.println(writer.toString());

    }
}
