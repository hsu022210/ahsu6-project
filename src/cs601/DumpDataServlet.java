package cs601;

/**
 * Created by hsu022210 on 10/30/16.
 */

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs601.hotelapp.HotelData;

/** ReviewsAPIServlet for the JettyHttpServer example */
@SuppressWarnings("serial")
public class DumpDataServlet extends HttpServlet {

    // DatabaseHandler interacts with the MySQL database
    private static final DatabaseHandler dbhandler = DatabaseHandler.getInstance();

    /**
     * A method that gets executed when the get request is sent to the
     * HelloServlet
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        response.setStatus(HttpServletResponse.SC_OK);

        PrintWriter out = response.getWriter();

        HotelData data = (HotelData) getServletContext().getAttribute("hotelData");
        Status status = dbhandler.dumpHotelAndReview(data);

        if (status == Status.OK){
            out.println("data dumped!");
        }else {
            out.println(status.message());
        }

    }
}
