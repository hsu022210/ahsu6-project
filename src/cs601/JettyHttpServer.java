package cs601;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs601.hotelapp.HotelData;
import cs601.hotelapp.HotelDataBuilder;
import cs601.hotelapp.ThreadSafeHotelData;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.apache.velocity.app.VelocityEngine;

/**
 * Created by hsu022210 on 10/30/16.
 */

/**
 * A simple web server that sends the same response for each GET request (an
 * html page that says Hello, friends!)
 *
 */

public class JettyHttpServer {

    public static final int PORT = 5000;

    public static void main(String[] args) throws Exception {
        Server server = new Server(5000);

        ThreadSafeHotelData data = new ThreadSafeHotelData();
        HotelDataBuilder hotelDataBuilder = new HotelDataBuilder(data);

        hotelDataBuilder.loadHotelInfo("input/hotels200.json");
        hotelDataBuilder.loadReviews(Paths.get("input/reviews"));

//        HotelData data = new HotelData();
//
//        data.loadHotelInfo("input/hotels200.json");
//        data.loadReviews(Paths.get("input/reviews"));

        // We could also setup the connector component explicitly
        //ServerConnector connector = new ServerConnector(server);
        //connector.setHost("localhost");
        //connector.setPort(PORT);
        //server.addConnector(connector);

        // initialize velocity
        VelocityEngine velocity = new VelocityEngine();
        velocity.init();

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.setAttribute("templateEngine", velocity);
        context.setAttribute("hotelData", data);
        context.setAttribute("hotelDataBuilder", hotelDataBuilder);
        context.addServlet(HotelInfoAPIServlet.class, "/hotelInfoAPI");
        context.addServlet(HotelListServlet.class, "/hotelList");
        context.addServlet(HotelListServlet.class, "/");
        context.addServlet(ReviewsAPIServlet.class, "/reviewsAPI");
        context.addServlet(RegistrationServlet.class, "/register");
        context.addServlet(LoginServlet.class, "/login");
        context.addServlet(LogoutServlet.class, "/logout");
        context.addServlet(AddReviewServlet.class, "/addReview");
        context.addServlet(HotelDetailServlet.class, "/hotelDetail");
        context.addServlet(ReviewsServlet.class, "/hotelReviews");
        context.addServlet(ModifyReviewServlet.class, "/modifyReview");
        context.addServlet(TouristAttractionsServlet.class, "/touristAttractions");
        context.addServlet(AddExpediaServlet.class, "/addExpedia");
        context.addServlet(ExpediaLinksServlet.class, "/expedia");
        context.addServlet(AddSavedHotelsServlet.class, "/addSavedHotels");
        context.addServlet(SavedHotelsServlet.class, "/savedHotels");
        context.addServlet(ClearExpediaServlet.class, "/clearExpedia");
        context.addServlet(ClearSavedHotelsServlet.class, "/clearSavedHotels");

//        context.addServlet(DumpDataServlet.class, "/dumpData");
//        ServletHandler handler = new ServletHandler();

        // when the user goes to http://localhost:8080/hello, the get request is
        // going to go to a HelloServlet
//        handler.addServletWithMapping(HotelInfoAPIServlet.class, "/hotelInfo");
//        handler.addServletWithMapping(ReviewsAPIServlet.class, "/reviews");

//        server.setHandler(handler);
        server.setHandler(context);

        server.start();
        server.join();
    }
}
